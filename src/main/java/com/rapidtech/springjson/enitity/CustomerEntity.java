package com.rapidtech.springjson.enitity;

import com.rapidtech.springjson.model.AddressModel;
import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.SchoolModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.*;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "customer_tab")
public class CustomerEntity {
    @Id
    @TableGenerator(name = "customer_id_generator", table = "sequence_tab",
            pkColumnName = "gen_name", valueColumnName = "gen_value",
            pkColumnValue = "customer_id", initialValue = 0, allocationSize = 0)
    @GeneratedValue(strategy = GenerationType.TABLE, generator = "customer_id_generator")
    private Long id;
    @Column(name = "fullName", length = 100, nullable = false)
    private String fullName;
    @Column(name = "gender", length = 10, nullable = false)
    private String gender;
    @Temporal(TemporalType.DATE)
    @Column(name = "date_of_birth")
    private Date dateOfBirth;

    @Column(name = "place_of_birth")
    private String placeOfBirth;

    // relasi dengan address
    @OneToMany(mappedBy = "customer", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<AddressEntity> address = new ArrayList<>();

    // relasi dengan schoold
    @OneToMany(mappedBy = "customer",  cascade = CascadeType.ALL, orphanRemoval = true)
    private List<SchoolEntity> schools = new ArrayList<>();

    public CustomerEntity(CustomerModel customer) {
        this.fullName = customer.getFullName();
        this.dateOfBirth = customer.getDateOfBirth();
        this.gender = customer.getGender();
//        for (AddressModel addressModel : customer.getAddress()){
//            this.address.add(new AddressEntity(addressModel));
//        }
//        for (SchoolModel schoolModel : customer.getSchools()){
//            this.schools.add(new SchoolEntity(schoolModel));
//        }
    }

    public CustomerEntity(long id, String fullName, String gender) {
        this.id = id;
        this.fullName = fullName;
        this.gender = gender;
    }

    public void addAddress(AddressEntity addressEntity){
        this.address.add(addressEntity);
        addressEntity.setCustomer(this);
    }
    public void addAddressList(List<AddressModel> listAddress){
        for (AddressModel address : listAddress){
            AddressEntity addressEntity = new AddressEntity(address);
            addAddress(addressEntity);
        }
    }

    public void addSchool(SchoolEntity schoolEntity){
        this.schools.add(schoolEntity);
        schoolEntity.setCustomer(this);
    }

    public void addSchoolList(List<SchoolModel> listSchool){
        for(SchoolModel school : listSchool){
            SchoolEntity schoolEntity = new SchoolEntity(school);
            addSchool(schoolEntity);
        }
    }
}

package com.rapidtech.springjson.service.Impl;

import com.rapidtech.springjson.enitity.AddressEntity;
import com.rapidtech.springjson.enitity.CustomerEntity;
import com.rapidtech.springjson.enitity.SchoolEntity;
import com.rapidtech.springjson.model.*;
import com.rapidtech.springjson.repository.AddressRepository;
import com.rapidtech.springjson.repository.CustomerRepository;
import com.rapidtech.springjson.repository.SchoolRepository;
import com.rapidtech.springjson.service.CustomerService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Slf4j
@Service
public class CustomerServiceImpl implements CustomerService {
    private CustomerRepository customerRepository;
    private AddressRepository addressRepository;
    private SchoolRepository schoolRepository;

    @Autowired
    public CustomerServiceImpl(CustomerRepository customerRepository, AddressRepository addressRepository, SchoolRepository schoolRepository) {
        this.customerRepository = customerRepository;
        this.addressRepository = addressRepository;
        this.schoolRepository = schoolRepository;
    }

    @Override
    public List<CustomerModel> getAll() {
        return null;
    }

    @Override
    public Optional<CustomerModel> getById(Long id) {
        return Optional.empty();
    }

    @Override
    public CustomerResponse saveAll(CustomerRequest request) {
        if (request.getCustomers().isEmpty()) {
            return new CustomerResponse();
        }
        CustomerResponse response = new CustomerResponse();
        int countSuccess = 0, countFail = 0;
        List<CustomerModel> customerModelList = new ArrayList<>();
        for (CustomerModel model : request.getCustomers()) {
            Optional<CustomerModel> customerModel = this.saveCustomer(model);
            if (customerModel.isPresent()) {
                customerModelList.add(model);
                countSuccess++;
            } else {
                countFail++;
            }
        }
        return new CustomerResponse(customerModelList, countSuccess, countFail);
    }

    @Override
    public Optional<CustomerModel> saveCustomer(CustomerModel model) {
        if (model == null) {
            return Optional.empty();
        }
        CustomerEntity customerEntity = new CustomerEntity(model);
        if(!model.getAddress().isEmpty()) {
            customerEntity.addAddressList(model.getAddress());
        }
        if(!model.getSchools().isEmpty()) {
            customerEntity.addSchoolList(model.getSchools());
        }
        try {
            this.customerRepository.save(customerEntity);
            return Optional.of(new CustomerModel(customerEntity));
        } catch (Exception e) {
            log.error("Failed to save customer. Error: {}", e.getMessage());
            return Optional.empty();
        }
    }

    @Override
    public Optional<CustomerModel> save(CustomerModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<CustomerModel> update(Long id, CustomerModel model) {
        return Optional.empty();
    }

    @Override
    public Optional<CustomerModel> delete(Long id) {
        return Optional.empty();
    }


//    @Override
//    public Optional<CustomerRequest> saveCustomer(CustomerRequest requestModel) {
//        if (requestModel == null){
//            return Optional.empty();
//        } else{
//            for (CustomerModel customerModel : requestModel.getCustomers()){
//                CustomerEntity customerEntity = new CustomerEntity(customerModel);
//                customerEntity.addAddressToList(customerModel.getAddress());
//                customerEntity.addSchoolToList(customerModel.getSchools());
//
//                try{
//                    customerRepository.save(customerEntity);
//                } catch (Exception e){
//                    log.error("Failed to save customers. Error: {}", e.getMessage());
//                }
//            }
//            return Optional.of(requestModel);
//        }
//    }
}

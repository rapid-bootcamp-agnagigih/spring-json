package com.rapidtech.springjson.service;

import com.rapidtech.springjson.model.*;

import java.util.List;
import java.util.Optional;

public interface CustomerService {
    List<CustomerModel> getAll();
    Optional<CustomerModel> getById(Long id);
    CustomerResponse saveAll(CustomerRequest request);
//    Optional<CustomerRequest> saveCustomer(CustomerRequest request);
    Optional<CustomerModel> saveCustomer(CustomerModel model);
    Optional<CustomerModel> save(CustomerModel model);
    Optional<CustomerModel> update(Long id, CustomerModel model);
    Optional<CustomerModel> delete(Long id);
}

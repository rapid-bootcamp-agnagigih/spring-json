package com.rapidtech.springjson.controller;

import com.rapidtech.springjson.model.CustomerRequest;
import com.rapidtech.springjson.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/customers")
public class CustomerController {
    private CustomerService service;
    @Autowired
    public CustomerController(CustomerService service) {
        this.service = service;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Object> saveCustomer(@RequestBody CustomerRequest requestModel){
        return ResponseEntity.ok().body(service.saveAll(requestModel));
    }

//    @PostMapping
//    public ResponseEntity<Object> save(@RequestBody CustomerRequestModel requestModel){
//        return ResponseEntity.ok().body(requestModel);
//    }
}

package com.rapidtech.springjson.service.Impl;

import com.rapidtech.springjson.enitity.AddressEntity;
import com.rapidtech.springjson.enitity.CustomerEntity;
import com.rapidtech.springjson.enitity.SchoolEntity;
import com.rapidtech.springjson.model.AddressModel;
import com.rapidtech.springjson.model.CustomerModel;
import com.rapidtech.springjson.model.SchoolModel;
import com.rapidtech.springjson.repository.CustomerRepository;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.*;
import java.util.stream.Collectors;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@Slf4j
@ExtendWith(MockitoExtension.class)
class CustomerServiceImplTest {
    @InjectMocks
    @Autowired
    private CustomerServiceImpl service;

    @Mock
    private CustomerRepository repository;
    private List<CustomerEntity> customerEntityList;

    @BeforeEach
    void setUp() {
        log.info("Setup run ...");
        customerEntityList = Arrays.asList(
                new CustomerEntity(1L, "Pelanggan 1", "Wanita"),
                new CustomerEntity(2L, "Pelanggan 2", "Pria"),
                new CustomerEntity(3L, "Pelanggan 3", "Wanita")
        );
    }

    @AfterEach
    void tearDown() {
        log.info("Setup clear ...");
    }

    @Test
    void getAll() {
        when(this.repository.findAll()).thenReturn(Collections.emptyList());
        List<CustomerModel> result = service.getAll();
        assertNotNull(result);
        assertEquals(0, result.size());

        // mocking
        when(this.repository.findAll()).thenReturn(customerEntityList);
        result = service.getAll();
        assertNotNull(result);
        assertEquals(3, result.size());
        assertEquals("Pelanggan 1", result.get(0).getFullName());
        assertEquals("Wanita", result.get(0).getGender());
    }

    @Test
    void getById() {
        Optional<CustomerModel> result = service.getById(0L);
        assertEquals(Optional.empty(), result);

        CustomerEntity customerEntity = new CustomerEntity(1L, "Penlanggan", "Wanita");
        Optional<CustomerEntity> optional = Optional.of(customerEntity);

        // mocking
        when(repository.findById(1L)).thenReturn(optional);
        result = service.getById(1L);

        assertTrue(result.isPresent());
        assertEquals(1L, result.get().getId());
        assertEquals("Pelanggan 1", result.get().getFullName());
        assertEquals("Wanita", result.get().getGender());
    }

    @Test
    void saveAll() {
    }

    @Test
    void saveCustomer() {
        Optional<CustomerModel> result = this.service.save(null);
        assertEquals(Optional.empty(), result);
        List<AddressModel> addressModels = Arrays.asList(
                new AddressModel(0L, "Address 1", "Jalan 1", "Desa 1", "Kecamanatan 1", "Kabupaten 1", "Provinsi 1"),
                new AddressModel(0L, "Address 2", "Jalan 2", "Desa 2", "Kecamanatan 2", "Kabupaten 2", "Provinsi 2")
        );
        List<SchoolModel> schoolModels = Arrays.asList(
                new SchoolModel(0L, "SD", "SD N 1", "SD"),
                new SchoolModel(0L, "SMP", "SMP N 1", "SMP"),
                new SchoolModel(0L, "SMA", "SMA N 1", "SMA")
        );

        // prepare data request
        CustomerModel model = new CustomerModel(1L, "Pelanggan 1", addressModels, "Pria", new Date(), "Kabupaten 1"
                ,schoolModels);

        // prepare response dari repository save
        CustomerEntity entity = new CustomerEntity(model);
        List<AddressEntity> addressEntities = addressModels.stream().map(AddressEntity::new).collect(Collectors.toList());
        entity.setAddress(addressEntities);

        List<SchoolEntity> schoolEntities = schoolModels.stream().map(SchoolEntity::new).collect(Collectors.toList());
        entity.setSchools(schoolEntities);

        // mocking
        when(this.repository.save(any(CustomerEntity.class))).thenReturn(entity);
        result = this.service.save(model);
        assertNotNull(result);
        assertEquals("Pelanggan", result.get().getFullName());
        assertEquals("Pria", result.get().getGender());
        assertEquals("Kabupaten 1", result.get().getPlaceOfBirth());

        // validasi address
        assertEquals(2, result.get().getAddress().size());
        assertEquals(3, result.get().getSchools().size());
    }

    @Test
    void save() {
    }

    @Test
    void update() {
    }

    @Test
    void delete() {
    }
}